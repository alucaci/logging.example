#include <Rcpp.h>
#include <RcppSpdlog>
#include <spdlog/stopwatch.h> 

#include <vector>

// [[Rcpp::export]]
void setLoggerCpp()
{
  //https://cran.r-project.org/web/packages/RcppSpdlog/vignettes/introduction.html
  std::string logname = "TheLogger";
  auto sp = spdlog::get(logname);
  if (sp == nullptr)
  {
    sp = spdlog::r_sink_mt(logname);
  }
  spdlog::set_default_logger(sp);
  
  //https://github.com/gabime/spdlog/wiki/3.-Custom-formatting
  //https://spdlog.docsforge.com/v1.x/3.custom-formatting/#pattern-flags
  spdlog::set_pattern("%l [%Y-%m-%d %H:%M:%S][thread %t] %v");
}

// [[Rcpp::export]]
void setLogLevelCpp(const std::string &name)
{
  spdlog::set_level(spdlog::level::from_str(name));
}

// [[Rcpp::export]]
void doSomethingCpp()
{
  spdlog::debug("doSomethingCpp - start");
  
  std::vector<int> v = {1, 2, 3};
  
  // https://github.com/fmtlib/fmt
  spdlog::info("v: [{}]", fmt::join(v, ", "));
  
  // https://github.com/gabime/spdlog/blob/v1.x/example/example.cpp
  spdlog::stopwatch sw;
  std::this_thread::sleep_for(std::chrono::milliseconds(123));
  spdlog::info("Stopwatch: {} seconds", sw);
}