# logging.example

This a dummy package to illustrate the usage of loggers in R and Rcpp code.

On the R side, the _futile.logger_ package is used, on the C++ side, the _spdlog_ is used.
The log level can be set on the R side.
